import pygame, sys, math, itertools, random
from pygame.locals import *
from pygame import gfxdraw

pygame.init()
SCREEN_WIDTH = 1440
SCREEN_HEIGHT = 900
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Elliptical pool')

clock = pygame.time.Clock() # Create a clock value that allows us to set the FPS value we want.
mouse_down = False
first_game = True
balls_to_add = 0

# The next variable represents if the user wants to quit the game (when the value is True) or not (when the value is False).
# Since we want the game to run we start it with as False.
done = False

# Set up the colors.
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
CYAN = (0, 255, 255)
YELLOW = (255, 255, 0)
MAGENTA = (255, 0, 255)
ball_colors = [RED, GREEN, BLUE, CYAN, YELLOW, MAGENTA]

GREEN_CLOTH = (60, 180, 60)
WOOD = (160, 102, 56)

# Mass of ball.
m_ball = 10

# Radius of ball.
r_ball = 20

# Coefficient of restitution. Collisions will be "explosive" if the coefficient is set to something greater than 1.
e_ball_ball = 0.96
e_ball_rail = 0.7

# Coefficient of sliding friction.
mu_ball_ball = 0.02
mu_ball_cloth = 0.02
mu_ball_rail = 0.2

e = 0.43 # Eccentricity of the ellipse.
b = 320 # Semi-minor axis.
a = b/math.sqrt(1-e**2) # Semi-major axis.
c = math.sqrt(a**2 - b**2) # Linear eccentricity.

w = 300
l = w*2

ball_sound1 = pygame.mixer.Sound('BALL_STRIKE_1.wav')
ball_sound2 = pygame.mixer.Sound('BALL_STRIKE_2.wav')
ball_sound3 = pygame.mixer.Sound('BALL_STRIKE_3.wav')
cue_sound1 = pygame.mixer.Sound('CUE_1.wav')
cue_sound2 = pygame.mixer.Sound('CUE_2.wav')
pocket_sound = pygame.mixer.Sound('POCKET_COLLISION.wav')

def blend_colors(color1, color2, multiplier):
        """ Take two RGB colors and return a new color in between. The multiplier value should be a number between 0 and 1. """
        r = int(color1[0]* multiplier + color2[0] * (1-multiplier))
        g = int(color1[1]* multiplier + color2[1] * (1-multiplier))
        b = int(color1[2]* multiplier + color2[2] * (1-multiplier))
        return r, g, b
            
def draw_aacircle(x, y, radius, color):
        """ Draw an anti-aliased circle at (x, y). """
        pygame.gfxdraw.aacircle(screen, int(x), int(y), int(radius), color)
        pygame.gfxdraw.filled_circle(screen, int(x), int(y), int(radius), color)

def draw_aaellipse(x, y, rx, ry, color):
        pygame.gfxdraw.aaellipse(screen, int(x), int(y), int(rx), int(ry), color)
        pygame.gfxdraw.filled_ellipse(screen, int(x), int(y), int(rx), int(ry), color)

def norm(x, y):
        return math.sqrt(x**2 + y**2)

def normalize(x, y):
        return x/norm(x,y), y/norm(x, y)

class Pocket:
        # Pocket object that represents the pockets of the pool table.
        def __init__(self, r, x, y):
                self.radius = r
                self.x = x
                self.y = y

        #The draw function will be responsible for drawing the pocket in the screen.
        def draw(self):
                #draw_aacircle(self.x, self.y, self.radius, blend_colors(BLACK, GREEN_CLOTH, 0.1))
                draw_aacircle(self.x, self.y, self.radius*0.9, blend_colors(BLACK, GREEN_CLOTH, 0.4))
                draw_aacircle(self.x, self.y, self.radius*0.8, BLACK)
                
class Rect_Table:
        def __init__(self):
                self.x = SCREEN_WIDTH/2
                self.y = SCREEN_HEIGHT/2
                self.l = l
                self.w = w
                self.c = l/2
                self.vertices = [(self.x - l, self.y - w), (self.x + l, self.y - w),
                                 (self.x + l, self.y + w), (self.x - l, self.y + w)]
                self.holes = self.vertices[:]
                self.holes.append((self.x, self.y - w))
                self.holes.append((self.x, self.y + w))

                
        def draw(self):
                for (x, y) in self.vertices:
                        draw_aacircle(x, y, 60, blend_colors(BLACK, WOOD, 0.5))
                        draw_aacircle(x, y, 57, WOOD)
                pygame.draw.polygon(screen, blend_colors(BLACK, WOOD, 0.5), self.vertices, 121)
                pygame.draw.polygon(screen, WOOD, self.vertices, 115)
                for (x, y) in self.holes:
                        draw_aacircle(x, y, 28, blend_colors(BLACK, WOOD, 0.5))
                pygame.draw.polygon(screen, blend_colors(BLACK, WOOD, 0.5), self.vertices, int(r_ball*1.6))
                pygame.draw.polygon(screen, blend_colors(BLACK, GREEN_CLOTH, 0.6), self.vertices, int(r_ball*1.3))
                pygame.draw.polygon(screen, blend_colors(BLACK, GREEN_CLOTH, 0.45), self.vertices, int(r_ball*1.2))
                pygame.draw.polygon(screen, blend_colors(BLACK, GREEN_CLOTH, 0.25), self.vertices, int(r_ball*0.9))
                pygame.draw.polygon(screen, blend_colors(BLACK, GREEN_CLOTH, 0.1), self.vertices, int(r_ball*0.6))
                pygame.draw.polygon(screen, GREEN_CLOTH, self.vertices)

class Elliptical_Table:
        """ Pool table object. """
        def __init__(self):
                self.x = SCREEN_WIDTH/2
                self.y = SCREEN_HEIGHT/2
                self.a = a
                self.b = b
                self.c = math.sqrt(a**2 - b**2)
                self.holes = [(self.x + c, self.y)]

        #The draw function will be responsible for drawing the table in the screen.
        def draw(self):
                draw_aaellipse(self.x, self.y, self.a + r_ball*4, self.b + r_ball*4 , blend_colors(BLACK, WOOD, 0.3))
                draw_aaellipse(self.x, self.y, self.a + r_ball*3.85, self.b + r_ball*3.85 , WOOD) 
                draw_aaellipse(self.x, self.y, self.a + r_ball*2.1, self.b + r_ball*2.1 , blend_colors(BLACK, WOOD, 0.2)) 
                draw_aaellipse(self.x, self.y, self.a + r_ball*2, self.b + r_ball*2, GREEN_CLOTH)

                # Shadow between rail and table surface.
                draw_aaellipse(self.x, self.y, self.a + r_ball*1, self.b + r_ball*1, blend_colors(BLACK, GREEN_CLOTH, 0.7))
                draw_aaellipse(self.x, self.y, self.a + r_ball*0.8, self.b + r_ball*0.8, blend_colors(BLACK, GREEN_CLOTH, 0.45))
                draw_aaellipse(self.x, self.y, self.a + r_ball*0.6, self.b + r_ball*0.6, blend_colors(BLACK, GREEN_CLOTH, 0.25))
                draw_aaellipse(self.x, self.y, self.a + r_ball*0.2, self.b + r_ball*0.2, blend_colors(BLACK, GREEN_CLOTH, 0.1))
                
                draw_aaellipse(self.x, self.y, self.a, self.b, GREEN_CLOTH)
                draw_aacircle(self.x - c, self.y, 5, WHITE) # Focus of the ellipse on the right.

class Ball:
        """ Billiard ball object. """
        def __init__(self, x, y, color):
                # Distance from top left corner.
                self.x = x
                self.y = y
                
                self.vx = 0
                self.vy = 0
                self.v = 0

                # Distance from center of table. This will be used for calculating the rebound angle from the rail of the table.
                self.X = self.x - pool_table.x
                self.Y = self.y - pool_table.y
                self.R = norm(self.X, self.Y)

                self.radius = r_ball
                self.color = color # Intrinsic color.
                self.draw_color = color # Color for drawing the ball.
                self.pocket = False # Ball is not pocketed by default.

        #The draw function will be responsible for drawing the ball in the screen.
        def draw(self):
                draw_aacircle(self.x, self.y, self.radius, blend_colors(BLACK, self.draw_color, 0.6))
                draw_aacircle(self.x, self.y, self.radius*0.9, blend_colors(BLACK, self.draw_color, 0.3))
                draw_aacircle(self.x+self.radius*0.1, self.y-self.radius*0.1, self.radius*0.8, self.draw_color)
                draw_aacircle(self.x+self.radius*0.25, self.y-self.radius*0.3, self.radius*0.3, blend_colors(WHITE, self.draw_color, 0.5))

        def move(self, dx, dy):
                self.x = self.x + dx
                self.y = self.y + dy
                self.X = self.x - pool_table.x
                self.Y = self.y - pool_table.y
                self.R = norm(self.X, self.Y)

        #The update function determines how the ball's velocity and position changes with each iteration.
        def update(self):
                if self.v > mu_ball_cloth:
                        self.move(self.vx, self.vy)
                        ratio = 1 - mu_ball_cloth/self.v
                        self.vx = self.vx * ratio
                        self.vy = self.vy * ratio
                        self.v = self.v * ratio
                        
                        self.X = self.x - pool_table.x
                        self.Y = self.y - pool_table.y
                        self.R = norm(self.X, self.Y)
                else:
                        self.vx = 0
                        self.vy = 0
                        self.v = 0
                
        def add_velocity(self, ux, uy):
                self.vx = self.vx + ux
                self.vy = self.vy + uy
                self.v = norm(self.vx, self.vy)

        def distance(self, other, option=0):
                """ Distance from the ball to another object.
                If no option is specified, return the distance between the two. """
                x = other.x - self.x
                y = other.y - self.y
                if option == 1:
                        return x
                if option == 2:
                        return y
                return norm(x, y)

def collision(vx, vy, nx, ny, e, mu):
        """ Compute the velocity after colliding with normal vector (nx, ny) with restitution e and friction mu. """
        a = abs(vx*nx + vy*ny) # Absolute value of inner product between (ux, uy) and (rx, ry).
        vnx = a*nx # Normal velocity is (vx, vy) dot (nx, ny)
        vny = a*ny
        vtx = vx - vnx 
        vty = vy - vny
        if norm(vtx, vty) > mu:
                vtx = vtx*(1-mu)
                vty = vty*(1-mu)
        else:
                vtx = 0
                vty = 0
        ux = vtx - vnx*e
        uy = vty - vny*e
        return ux, uy

def ball_collision():
        """ Check for collisions among all balls on the table, and then adjust their velocities and positions after collision. """
        for (ball1, ball2) in itertools.combinations(ball_list, 2):
                if not ball1.pocket and not ball2.pocket: #Choose a combination of two balls on the table.
                        d = ball1.distance(ball2)
                        r = ball1.radius + ball2.radius
                        if d < r-0.1 and d != 0:
                                n = random.randint(1, 3)
                                if n == 1:
                                        ball_sound1.play()
                                if n == 2:
                                        ball_sound2.play()
                                else:
                                        ball_sound3.play()
                                # Find the unit vector (rx, ry) from ball1 to ball2
                                rx = ball1.distance(ball2, 1)/d
                                ry = ball1.distance(ball2, 2)/d
                                # Move their positions to avoid collision again.
                                ball1.move(-rx*(r-d)/2, -ry*(r-d)/2)
                                ball2.move(rx*(r-d)/2, ry*(r-d)/2)
                                # Change to center of velocity frame. Then ball1 has velocity (ux, uy), ball2 has velocity (-ux, -uy).
                                v1x = ball1.vx
                                v1y = ball1.vy
                                v2x = ball2.vx
                                v2y = ball2.vy                                
                                ux = (v1x-v2x)/2
                                uy = (v1y-v2y)/2
                                # Compute the velocity of ball1 after collision.
                                w = collision(ux, uy, rx, ry, e_ball_ball, mu_ball_ball)
                                # Change back to table frame.
                                ball1.vx = w[0] + (v1x + v2x)/2
                                ball1.vy = w[1] + (v1y + v2y)/2
                                ball1.v = norm(ball1.vx, ball1.vy)
                                ball2.vx = -w[0] + (v1x + v2x)/2
                                ball2.vy = -w[1] + (v1y + v2y)/2
                                ball2.v = norm(ball2.vx, ball2.vy)

def rail_collision():
        """ Check for collisions of all balls with the rail, and then adjust their velocities and positions after collision. """
        for ball in ball_list:
                if not ball.pocket:
                        if isinstance(pool_table, Rect_Table):
                                if ball.X > pool_table.l:
                                        ball.move(pool_table.l - ball.X, 0)
                                        ball.vx = -ball.vx*e_ball_rail
                                        ball.v = norm(ball.vx, ball.vy)
                                if ball.X < -pool_table.l:
                                        ball.move(-pool_table.l - ball.X, 0)
                                        ball.vx = -ball.vx*e_ball_rail
                                        ball.v = norm(ball.vx, ball.vy)
                                if ball.Y > pool_table.w:
                                        ball.move(0, pool_table.w - ball.Y)
                                        ball.vy = -ball.vy*e_ball_rail
                                        ball.v = norm(ball.vx, ball.vy)
                                if ball.Y < -pool_table.w:
                                        ball.move(0, -pool_table.w - ball.Y)
                                        ball.vy = -ball.vy*e_ball_rail
                                        ball.v = norm(ball.vx, ball.vy)
                        if isinstance(pool_table, Elliptical_Table):
                                X = ball.X
                                Y = ball.Y
                                R = ball.R
                                a = pool_table.a
                                b = pool_table.b
                                c = pool_table.c
                                d = norm(X-c, Y) + norm(X+c, Y) # Distance of the ball from the two foci.
                                if d > a+a: # If the ball is outside the ellipse
                                        theta = math.atan2(Y,X) # Radial angle from semi-major axis.
                                        r = a*b/norm(b*math.cos(theta), a*math.sin(theta)) # Radial distance from center.
                                        dx = X - r*math.cos(theta) # Calculate the x and y distance from the edge of the ellipse.
                                        dy = Y - r*math.sin(theta)
                                        ball.move(-dx, -dy) # Move the ball to the edge of the table.
                                        
                                        vx = ball.vx
                                        vy = ball.vy
                                        if Y == 0: # If the ball collides with the far right edge of the ellipse.
                                                ball.vx = -vx * e_ball_rail # Simply reverse the x-velocity.
                                                ball.v = norm(vx, vy)
                                                
                                        else:
                                                m = -(b**2/a**2)*(X/Y) # Slope of the tangent.
                                                (nx, ny) = normalize(m, -1) # Normal unit vector (nx, ny)
                                                if Y > 0:
                                                        (nx, ny) = (-nx, -ny)
                                                u = collision(vx, vy, nx, ny, e_ball_rail, mu_ball_rail)
                                                ball.vx = u[0]
                                                ball.vy = u[1]
                                                ball.v = norm(ball.vx, ball.vy)

def pocket_ball(ball, pocket):
        """ Check if the ball is near the pocket, then adjust the balls properties accordingly."""
        r = pocket.radius
        d = ball.distance(pocket)
        if d <= r/2: # Force the ball into the pocket if too deep into the pocket.
                pocket_sound.play()
                pocketed_balls.append(ball)
                ball.pocket = True
                ball.x = pocket.x
                ball.y = pocket.y
                ball.vx = 0
                ball.vy = 0
                ball.v = 0
                ball.draw_color = blend_colors(BLACK, ball.color, 0.7) # Add shading to the ball.
        elif d <= r: # If the ball is near the rim of the pocket.
                x = ball.distance(pocket, 1)
                y = ball.distance(pocket, 2)
                ax = 2*r*x/d**2
                ay = 2*r*y/d**2
                ball.vx = ball.vx * 0.5
                ball.vy = ball.vy * 0.5
                ball.add_velocity(ax, ay) # Generate a pulling force towards the pocket's center.

def main():
        """ Main loop. """
        global mouse_down, balls_to_add
        for event in pygame.event.get():
                mouse_pos = pygame.mouse.get_pos()
                if event.type == QUIT:
                        done = True
                        pygame.quit()
                        sys.exit()
                elif event.type == MOUSEBUTTONDOWN and on_cue_ball(mouse_pos): # Detect whether the user clicks on the cue ball.
                        mouse_down = True
                elif event.type == MOUSEBUTTONUP and mouse_down: 
                        mouse_down = False
                        if can_shoot(): # If can shoot, detect where the user releases the mouse button, then shoots the ball with the corresponding velocity.
                                n = random.randint(1, 2)
                                if n == 1:
                                        cue_sound1.play()
                                else:
                                        cue_sound2.play()
                                mouse_x, mouse_y = event.pos
                                force_x = int(cue_ball.x) - mouse_x
                                force_y = int(cue_ball.y) - mouse_y
                                cue_ball.add_velocity(force_x / m_ball, force_y / m_ball)

        screen.fill(BLACK)
        pool_table.draw()
        ball_collision()
        rail_collision()

        for pocket in pocket_list:
                pocket.draw()
                for ball in ball_list: # Detect if the ball is pocketed.
                        if not ball.pocket:
                                pocket_ball(ball, pocket)
                                if isinstance(pool_table, Elliptical_Table):
                                        # If a colored ball is pocketed, add another random colored ball when the other balls have stopped.
                                        if ball.pocket and ball != cue_ball:
                                                balls_to_add += 1

        if can_shoot():
                if cue_ball.pocket:
                        # Reset the cue ball after all balls have stopped.
                        pocketed_balls.remove(cue_ball)
                        cue_ball.x = pool_table.x - pool_table.c
                        cue_ball.y = pool_table.y
                        cue_ball.draw_color = cue_ball.color
                        cue_ball.pocket = False
                while balls_to_add > 0:
                        add_rand_ball()
                        balls_to_add -= 1
        
        for ball in pocketed_balls:
                ball.draw()
        
        for ball in ball_list:
                if not ball.pocket:
                        ball.update()
                        ball.draw()
        
        if mouse_down: # Draw the predicted path of the cue ball while the user is aiming.
                x = int(cue_ball.x)
                y = int(cue_ball.y)
                mouse_x, mouse_y = pygame.mouse.get_pos()
                dx = x - mouse_x
                dy = y - mouse_y
                dr = norm(dx, dy)
                if  dr!= 0:
                        pygame.draw.aaline(screen, WHITE, (x, y), (x + int(dx/dr*SCREEN_WIDTH), y + int(dy/dr*SCREEN_WIDTH)))

def on_cue_ball(pos):
        """ Detect whether the cursor is hovering over the cue ball. """
        x = pos[0]
        y = pos[1]
        dx = x - cue_ball.x
        dy = y - cue_ball.y
        r = cue_ball.radius
        return r**2 > dx**2 + dy**2

def can_shoot():
        """ Detect whether all balls have stopped on the table. """
        for ball in ball_list:
                if ball.v != 0:
                        return False
        return True

def all_clear():
        """ Detect whether all balls except the cue ball have been pocketed. """
        if first_game:
                return True # Bypass this function when first starting the game.
        for ball in ball_list[1:]:
                if not ball.pocket:
                        return False
        return can_shoot() and isinstance(pool_table, Rect_Table) and not cue_ball.pocket

def add_rand_ball():
        """ Add a new ball of random color at a random position on the table. """
        x = pool_table.x
        y = pool_table.y
        A = a*0.8
        p1 = random.uniform(-A, A)
        B = b * math.sqrt(1 - (p1/a)**2)*0.8
        p2 = random.uniform(-B, B)
        ball_list.append(Ball(x + p1, y + p2, ball_colors[random.randint(0, len(ball_colors)-1)]))

def table_init():
        """ Set up the balls on the tables for a new game. """
        global cue_ball
        del ball_list[:]
        del pocketed_balls[:]
        cue_ball = Ball(pool_table.x - pool_table.c, pool_table.y, (230, 230, 230))
        ball_list.append(cue_ball)
        if isinstance(pool_table, Rect_Table):
                y = pool_table.y
                x = pool_table.x + pool_table.c
                r = r_ball
                h = r*math.sqrt(3)
                ball_list.append(Ball(x, y, RED))
                ball_list.append(Ball(x + h, y + r, GREEN))
                ball_list.append(Ball(x + h, y - r, BLUE))
                ball_list.append(Ball(x + h*2, y, (40, 40, 40)))
                ball_list.append(Ball(x + h*2, y + r*2, CYAN))
                ball_list.append(Ball(x + h*2, y - r*2, MAGENTA))
                ball_list.append(Ball(x + h*3, y + r*3, GREEN))
                ball_list.append(Ball(x + h*3, y + r, BLUE))
                ball_list.append(Ball(x + h*3, y - r, RED))
                ball_list.append(Ball(x + h*3, y - r*3, YELLOW))
        if isinstance(pool_table, Elliptical_Table):
                add_rand_ball()

pool_table = Elliptical_Table()
ball_list = []
pocketed_balls = []
pocket_list = []
for (x, y) in pool_table.holes:
        pocket_list.append(Pocket(28, x, y))

# While the user doesn't quit.
while not done:
        if all_clear():
                table_init()
                first_game = False
        main()
        pygame.display.update()
        clock.tick(60)
